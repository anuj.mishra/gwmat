#!/bin/bash

rm -r docs/

sphinx-quickstart docs <<EOF
n
pass
pass
pass
en
EOF

cp conf.py docs/
cp index.rst docs/
sphinx-apidoc -o docs ../gwmat/
#cp modules.rst docs/
cd docs
make html
cd ..
