gwmat package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   gwmat.bilby_custom_FD_source_models
   gwmat.methods

Submodules
----------

gwmat.constants module
----------------------

.. automodule:: gwmat.constants
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.conversion module
-----------------------

.. automodule:: gwmat.conversion
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.cosmology module
----------------------

.. automodule:: gwmat.cosmology
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.cythonized\_point\_lens module
------------------------------------

.. automodule:: gwmat.cythonized_point_lens
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.domain module
-------------------

.. automodule:: gwmat.domain
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.general\_utils module
---------------------------

.. automodule:: gwmat.general_utils
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.gw\_utils module
----------------------

.. automodule:: gwmat.gw_utils
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.injection module
----------------------

.. automodule:: gwmat.injection
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.nr\_injection module
--------------------------

.. automodule:: gwmat.nr_injection
   :members:
   :undoc-members:
   :show-inheritance:

gwmat.point\_lens module
------------------------

.. automodule:: gwmat.point_lens
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: gwmat
   :members:
   :undoc-members:
   :show-inheritance:
