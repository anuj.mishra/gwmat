# GWMAT: Gravitational Waves Microlensing Analysis Tools

## Introduction

GWMAT is a python/cython package containing various useful tools for the study of microlensing (ML) of Gravitational Wave (GW) signals. Additionally, the package can also be used for analysing or injecting unlensed signals.

## Documentation
Documentation can be found at this link: [GWMAT Documentation](https://anuj.mishra.docs.ligo.org/gwmat/)


## Installation
The easiest way to install `gwmat` is by first cloning this git repository:
```
git clone git@git.ligo.org:anuj.mishra/gwmat.git
```

 Then go to the cloned repository and run :
```
sh ./install.sh
```
